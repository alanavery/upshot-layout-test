'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var productData = [{
  img: 'images/product-shoe.png',
  title: 'Title with Two Very Very Long Long Lines',
  description: 'And a very long desciption to boot. Why bother descibing something with so many words?'
}, {
  img: 'images/product-watch.png',
  title: 'One Short Title',
  description: 'And a very long desciption to boot. Why bother descibing something with so many words?'
}, {
  img: 'images/product-shoe.png',
  title: 'One Short Title',
  description: 'With a short description.'
}, {
  img: 'images/product-bike.png',
  title: 'Title with Two Very Very Long Long Lines',
  description: 'With a short description.'
}, {
  img: 'images/product-shoe.png',
  title: 'Title with Two Very Very Long Long Lines',
  description: 'And a very long desciption to boot.  Why bother descibing something with so many words?'
}, {
  img: 'images/product-watch.png',
  title: 'Title with Two Very Very Long Long Lines',
  description: 'And a very long desciption to boot.  Why bother descibing something with so many words?'
}];

var ProductListing = function (_React$Component) {
  _inherits(ProductListing, _React$Component);

  function ProductListing() {
    _classCallCheck(this, ProductListing);

    return _possibleConstructorReturn(this, (ProductListing.__proto__ || Object.getPrototypeOf(ProductListing)).apply(this, arguments));
  }

  _createClass(ProductListing, [{
    key: 'render',
    value: function render() {
      var productCards = productData.map(function (product) {
        return React.createElement(
          'div',
          { className: 'div-card' },
          React.createElement(
            'div',
            { className: 'div-card-content' },
            React.createElement('img', { src: product.img, alt: '' }),
            React.createElement(
              'h2',
              null,
              product.title
            ),
            React.createElement(
              'p',
              null,
              product.description
            )
          ),
          React.createElement(
            'button',
            null,
            'Buy Now'
          )
        );
      });

      return React.createElement(
        'div',
        { className: 'div-product-listing' },
        productCards
      );
    }
  }]);

  return ProductListing;
}(React.Component);

var container = document.querySelector('.container-product-listing');
ReactDOM.render(React.createElement(ProductListing, null), container);