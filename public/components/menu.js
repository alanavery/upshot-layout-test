'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Menu = function (_React$Component) {
  _inherits(Menu, _React$Component);

  function Menu() {
    _classCallCheck(this, Menu);

    return _possibleConstructorReturn(this, (Menu.__proto__ || Object.getPrototypeOf(Menu)).apply(this, arguments));
  }

  _createClass(Menu, [{
    key: 'render',
    value: function render() {
      return React.createElement(
        'div',
        { className: 'div-menu' },
        React.createElement(
          'ul',
          null,
          React.createElement(
            'a',
            null,
            React.createElement(
              'li',
              null,
              'Footwear'
            )
          ),
          React.createElement(
            'a',
            null,
            React.createElement(
              'li',
              null,
              'Water Fountains'
            )
          ),
          React.createElement(
            'a',
            null,
            React.createElement(
              'li',
              null,
              'Garage Door Openers'
            )
          ),
          React.createElement(
            'a',
            null,
            React.createElement(
              'li',
              null,
              'Smart Home Tech'
            )
          ),
          React.createElement(
            'a',
            null,
            React.createElement(
              'li',
              null,
              'Beverages'
            )
          ),
          React.createElement(
            'a',
            null,
            React.createElement(
              'li',
              null,
              'Education'
            )
          )
        )
      );
    }
  }]);

  return Menu;
}(React.Component);

var container = document.querySelector('.container-menu');
ReactDOM.render(React.createElement(Menu, null), container);