'use strict';

let productData = [
  {
    img: 'images/product-shoe.png',
    title: 'Title with Two Very Very Long Long Lines',
    description: 'And a very long desciption to boot. Why bother descibing something with so many words?',
  },
  {
    img: 'images/product-watch.png',
    title: 'One Short Title',
    description: 'And a very long desciption to boot. Why bother descibing something with so many words?',
  },
  {
    img: 'images/product-shoe.png',
    title: 'One Short Title',
    description: 'With a short description.',
  },
  {
    img: 'images/product-bike.png',
    title: 'Title with Two Very Very Long Long Lines',
    description: 'With a short description.',
  },
  {
    img: 'images/product-shoe.png',
    title: 'Title with Two Very Very Long Long Lines',
    description: 'And a very long desciption to boot.  Why bother descibing something with so many words?',
  },
  {
    img: 'images/product-watch.png',
    title: 'Title with Two Very Very Long Long Lines',
    description: 'And a very long desciption to boot.  Why bother descibing something with so many words?',
  },
];

class ProductListing extends React.Component {
  render() {
    let productCards = productData.map((product) => {
      return (
        <div className="div-card">
          <div className="div-card-content">
            <img src={product.img} alt="" />
            <h2>{product.title}</h2>
            <p>{product.description}</p>
          </div>
          <button>Buy Now</button>
        </div>
      );
    });

    return <div className="div-product-listing">{productCards}</div>;
  }
}

let container = document.querySelector('.container-product-listing');
ReactDOM.render(<ProductListing />, container);
