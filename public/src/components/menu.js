'use strict';

class Menu extends React.Component {
  render() {
    return (
      <div className="div-menu">
        <ul>
          <a>
            <li>Footwear</li>
          </a>
          <a>
            <li>Water Fountains</li>
          </a>
          <a>
            <li>Garage Door Openers</li>
          </a>
          <a>
            <li>Smart Home Tech</li>
          </a>
          <a>
            <li>Beverages</li>
          </a>
          <a>
            <li>Education</li>
          </a>
        </ul>
      </div>
    );
  }
}

let container = document.querySelector('.container-menu');
ReactDOM.render(<Menu />, container);
