'use strict';

class Header extends React.Component {
  render() {
    return (
      <div className="div-header">
        <a className="link-logo">
          <img src="images/logo.png" alt="" />
        </a>
        <nav>
          <ul>
            <li>
              <a>Home</a>
            </li>
            <li>
              <a>About</a>
            </li>
            <li>
              <a>Products</a>
            </li>
            <li>
              <a>Help</a>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

let container = document.querySelector('.container-header');
ReactDOM.render(<Header />, container);
